<?php

/**
 * Definition of Quiz Web Service
 *
 * @package    local_quiz_ws
 * @copyright  2015 Martin Náhlovský
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_duplicate_quiz' => array(
                'classname'   => 'local_quiz_external',
                'methodname'  => 'duplicate_quiz',
                'classpath'   => 'local/moodle_quiz_ws/externallib.php',
                'description' => 'Duplicates quiz by given quiz id and course id.',
                'type'        => 'write',
        ),
        'local_edit_quiz' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'edit_quiz',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Edit quiz by given id.',
            'type'        => 'write',
        ),
        'local_get_quizes' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'get_quizes',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Get quizes by given course id.',
            'type'        => 'read',
        ),
        'local_get_quiz_grades' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'get_quiz_grades',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Get grades by given course id.',
            'type'        => 'read',
        ),
        'local_get_course_sections' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'get_course_sections',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Get sections by given course id.',
            'type'        => 'read',
        ),
        'local_set_quiz_visibility' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'set_quiz_visibility',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Set visibility of a quiz.',
            'type'        => 'read',
        ),
        'local_get_courses' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'get_courses',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Get courses.',
            'type'        => 'read',
        ),
        'local_delete_quiz' => array(
            'classname'   => 'local_quiz_external',
            'methodname'  => 'delete_quiz',
            'classpath'   => 'local/moodle_quiz_ws/externallib.php',
            'description' => 'Delete quiz.',
            'type'        => 'write',
        ),
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Quiz Web Service' => array(
                'functions' => array (
                    'local_duplicate_quiz',
                    'local_edit_quiz',
                    'local_get_quizes',
                    'local_get_quiz_grades',
                    'local_get_course_sections',
                    'local_set_quiz_visibility',
                    'local_get_courses',
                    'local_delete_quiz'
                ),
                'restrictedusers' => 1,
                'enabled' => 1,
        )
);
