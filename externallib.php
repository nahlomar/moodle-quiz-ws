<?php

/**
 * External Quiz Web Service
 *
 * @package    local_quiz_ws
 * @copyright  2015 Martin Náhlovský
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_quiz_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function duplicate_quiz_parameters() {
        // Parameters are equivalent to duplicate quiz function
        return self::edit_quiz_parameters();
    }

    /**
     * Returns quiz id
     * @return int quiz id
     */
    public static function duplicate_quiz($quizid, $options = array()) {
        global $USER, $CFG, $DB;
        require_once("$CFG->dirroot/course/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::duplicate_quiz_parameters(),
            array('quizid' => $quizid, 'options' => $options));

        $quizid = $params['quizid'];

        $transaction = $DB->start_delegated_transaction(); //If an exception is thrown in the below code, all DB queries in this code will be rollback.

        // Get course_module record
        $cm = get_coursemodule_from_instance('quiz', $quizid);

        // Get course record
        $course = $DB->get_record('course', array('id' => $cm->course));

        // Do duplicate quiz
        $newcm = duplicate_module($course, $cm);

        // Edit newly created quiz
        self::edit_quiz($newcm->instance, $params['options']);

        $transaction->allow_commit();

        return $newcm ? $newcm->instance : null;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function duplicate_quiz_returns() {
        return new external_value(PARAM_INT, 'quiz record id');
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function edit_quiz_parameters() {
        return new external_function_parameters(
            array('quizid' => new external_value(PARAM_INT, 'id of quiz to duplicate'),
                'options' =>
                    new external_single_structure(
                        array(
                            'name' => new external_value(PARAM_TEXT, 'name of quiz'),
                            'timeopen' => new external_value(PARAM_INT, 'quiz open time'),
                            'timeclose' => new external_value(PARAM_INT, 'quiz end time'),
                            'password' => new external_value(PARAM_TEXT, 'password of quiz'),
                            'sectionid' => new external_value(PARAM_INT, 'section id of quiz'),
                            'visibility' => new external_value(PARAM_INT, 'visibility of quiz'),
                        )
                )
            )
        );
    }

    /**
     * Returns boolean of update success or failure
     * @return int quiz id
     */
    public static function edit_quiz($quizid, $options = array()) {
        global $CFG, $DB;
        $quizconfig = get_config('quiz');
        require_once("$CFG->dirroot/mod/quiz/lib.php");
        require_once("$CFG->dirroot/course/modlib.php");

        //Parameter validation
        $params = self::validate_parameters(self::edit_quiz_parameters(),
                        array('quizid' => $quizid, 'options' => $options));

        $options = $params['options'];
        $quizid = $params['quizid'];

        $transaction = $DB->start_delegated_transaction(); //If an exception is thrown in the below code, all DB queries in this code will be rollback.

        // Get course_module record of quiz
        $cm = get_coursemodule_from_instance('quiz', $quizid);
//        $cm = get_fast_modinfo(0)->instances['quiz'][$id];

        $quiz = new stdClass();

        // Set values
        $quiz->coursemodule = $cm->id;
        $quiz->course = $cm->course;
        $quiz->instance = $quizid;
        $quiz->name =$options['name'];
        $quiz->timeopen = $options['timeopen'];
        $quiz->timeclose = $options['timeclose'];
        $quiz->quizpassword = empty($options['password']) ? $quizconfig->password : $options['password'];

        // Update quiz
        $update = quiz_update_instance($quiz, null);

        // Update section if sectionid is set
        if($options['sectionid'] && $cm->section != $options['sectionid']) {

            // Get section record
            if(!$section = $DB->get_record('course_sections', array('id' => $options['sectionid']), '*', MUST_EXIST)) {
                throw new invalid_parameter_exception('Section with id ' . $options['sectionid'] . ' doesn\'t exist.');
            }
            moveto_module($cm, $section);
        }

        // Update visibility
        if(!is_null($options['visibility'])) {
            self::set_quiz_visibility($quizid, $options['visibility']);
        }

        $transaction->allow_commit();

        rebuild_course_cache($cm->course, true);

        return $update;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function edit_quiz_returns() {
        return new external_value(PARAM_INT, 'quiz update success/failure');
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_quizes_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'id of course'),
                'sectionid' => new external_value(PARAM_INT, 'id of section'),
            )
        );
    }

    /**
     * Returns list of quizes
     * @return array quizes
     */
    public static function get_quizes($courseid, $sectionid) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/mod/quiz/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::get_quizes_parameters(), array('courseid' => $courseid, 'sectionid' => $sectionid));

        // Get the course_modules records as an associative array
        $cms = $DB->get_records_menu('course_modules', array('course' => $params['courseid'], 'section' => $params['sectionid']), 'id', 'id,instance');

        $return = array();

        $quizes = $DB->get_records_list('quiz', 'id', $cms);

//        $DB->get_records_sql("
//            SELECT *
//                FROM {course_modules} cm
//                  LEFT JOIN {quiz} q ON q.
//        ");

        foreach($quizes as $quiz) {
            $return[] = array(
                'id' => $quiz->id,
                'name' => $quiz->name,
                'timeopen' => $quiz->timeopen,
                'timeclose' => $quiz->timeclose,
                'password' => $quiz->password,
            );
        }

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_quizes_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'group record id'),
                    'name' => new external_value(PARAM_TEXT, 'quiz name'),
                    'timeopen' => new external_value(PARAM_INT, 'quiz open time'),
                    'timeclose' => new external_value(PARAM_INT, 'quiz close time'),
                    'password' => new external_value(PARAM_TEXT, 'quiz password'),
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_quiz_grades_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id of quiz'),
                'timestart' => new external_value(PARAM_INT, 'start time of quiz attempts'),
            )
        );
    }

    /**
     * Returns list of grades
     * @return array grades
     */
    public static function get_quiz_grades($id, $timestart) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/mod/quiz/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::get_quiz_grades_parameters(), array('id' => $id, 'timestart' => $timestart));

        $cm = get_coursemodule_from_instance('quiz', $params['id']);

        $activities = array();
        $index = 0;
        quiz_get_recent_mod_activity($activities, $index, $timestart, $cm->course, $cm->id);

        foreach($activities as $activity) {
            $return[] = array(
                'userid' => $activity->user->id,
                'timestamp' => $activity->timestamp,
                'sumgrades' => $activity->content->sumgrades,
                'maxgrade' => $activity->content->maxgrade,

            );
        }

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_quiz_grades_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'userid' => new external_value(PARAM_INT, 'user id'),
                    'timestamp' => new external_value(PARAM_INT, 'end time of attempt'),
                    'sumgrades' => new external_value(PARAM_INT, 'summary grades'),
                    'maxgrade' => new external_value(PARAM_INT, 'maximum grade'),
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_course_sections_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id of course'),
            )
        );
    }

    /**
     * Returns list of sections by given course id
     * @return array grades
     */
    public static function get_course_sections($id) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/mod/quiz/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::get_course_sections_parameters(), array('id' => $id));

        $sections = $DB->get_records('course_sections', array('course' => $params['id']));

        foreach($sections as $section) {
            $return[] = array(
                'id' => $section->id,
                'name' => $section->name,
            );
        }

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_course_sections_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'section id'),
                    'name' => new external_value(PARAM_TEXT, 'name of section'),
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function set_quiz_visibility_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id of course'),
                'visibility' => new external_value(PARAM_INT, 'visibility of quiz'),
            )
        );
    }

    /**
     * Returns true/false
     * @return int result
     */
    public static function set_quiz_visibility($id, $visibility) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/course/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::set_quiz_visibility_parameters(), array('id' => $id, 'visibility' => $visibility));

        $cm = get_coursemodule_from_instance('quiz', $params['id']);

        $return = set_coursemodule_visible($cm->id, $visibility);

        $DB->set_field('course_modules', 'visibleold', $cm->visible, array('id' => $cm->id));

        \core\event\course_module_updated::create_from_cm($cm)->trigger();

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function set_quiz_visibility_returns() {
        return new external_value(PARAM_INT, 'quiz update visibility success/failure');
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_courses_parameters() {
        return new external_function_parameters(array());
    }

    /**
     * Returns list of courses
     * @return array courses
     */
    public static function get_courses() {
        global $CFG, $DB;
        require_once("$CFG->dirroot/course/lib.php");

        $return = array();

        $courses = $DB->get_records('course');

        foreach($courses as $course) {
            $return[] = array(
                'id' => $course->id,
                'name' => $course->shortname,
            );
        }

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'course id'),
                    'name' => new external_value(PARAM_TEXT, 'course name'),
                )
            )
        );
    }


    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function delete_quiz_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'id of quiz'),
            )
        );
    }

    /**
     * Returns success/fail of delete
     * @return array courses
     */
    public static function delete_quiz($id) {
        global $CFG, $DB;
        require_once("$CFG->dirroot/mod/quiz/lib.php");

        //Parameter validation
        $params = self::validate_parameters(self::delete_quiz_parameters(), array('id' => $id));

        $transaction = $DB->start_delegated_transaction(); //If an exception is thrown in the below code, all DB queries in this code will be rollback.

        $return = quiz_delete_instance($params['id']);

        $transaction->allow_commit();

        rebuild_course_cache($cm->course, true);

        return $return;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function delete_quiz_returns() {
        return new external_value(PARAM_BOOL, 'status: true if success');
    }

}
