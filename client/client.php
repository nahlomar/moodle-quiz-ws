<?php
// This client for local_wstemplate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * XMLRPC client for Moodle 2 - local_wstemplate
 *
 * This script does not depend of any Moodle code,
 * and it can be called from a browser.
 *
 * @authorr Jerome Mouneyrac
 */

/// MOODLE ADMINISTRATION SETUP STEPS
// 1- Install the plugin
// 2- Enable web service advance feature (Admin > Advanced features)
// 3- Enable XMLRPC protocol (Admin > Plugins > Web services > Manage protocols)
// 4- Create a token for a specific user and for the service 'My service' (Admin > Plugins > Web services > Manage tokens)
// 5- Run this script directly from your browser: you should see 'Hello, FIRSTNAME'

/// SETUP - NEED TO BE CHANGED
$token = 'd9e34d08662bdf763d4e83907c4a1b8c'; // Quiz Web Service
//$token = '97ec9ba739ba95aab12aca33c4ed65df';  // Course Web Service
$domainname = 'http://moodle';

///// XML-RPC QUIZ DUP CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request("local_duplicate_quiz", array(24, array('name' => 'Vzor Test 50 - pokus 1 - dup6a', 'timeopen' => strtotime(date('2015-09-15 18:11:31')), 'timeclose' => strtotime(date('2015-09-15 18:11:31')), 'password' => 'test', 'sectionid' => 2, 'visibility' => 0)));
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);

///// XML-RPC EDIT QUIZ CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$params =array(45, array('name' => 'XXXX', 'timeopen' => strtotime(date('2015-09-15 18:11:31')), 'timeclose' => strtotime(date('2015-09-15 18:11:31')), 'password' => '', 'sectionid' => 2, 'visibility' => 0));
//$post = xmlrpc_encode_request('local_edit_quiz', $params);
//$resp = xmlrpc_decode($curl->post($serverurl, $post));

/////// XML-RPC GET QUIZES CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('local_get_quizes', array(2, 2));
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);

///// XML-RPC GET Grades CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('local_get_quiz_grades', array(8, 1442544125));
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);


///// XML-RPC GET COURSES CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('core_course_get_courses', array());
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);

//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('local_get_courses', array());
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);


///// XML-RPC GET COURSE SECTIONS CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('local_get_course_sections', array(2));
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);

///// XML-RPC SET QUIZ VISIBILITY CALL
//header('Content-Type: text/plain');
//$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
//require_once('./curl.php');
//$curl = new curl;
//$post = xmlrpc_encode_request('local_set_quiz_visibility', array(24,0));
//$resp = xmlrpc_decode($curl->post($serverurl, $post));
//print_r($resp);


///// XML-RPC DELETE QUIZ CALL
header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
require_once('./curl.php');
$curl = new curl;
$post = xmlrpc_encode_request('local_delete_quiz', array(1));
$resp = xmlrpc_decode($curl->post($serverurl, $post));
print_r($resp);
